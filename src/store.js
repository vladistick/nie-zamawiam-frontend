import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      step: null,
      status: null,
      queue: null,
  },
  mutations: {
      setStep (state, step)
      {
          state.step = step
      },
      setStatus (state, status)
      {
          state.status = status
      },
      setQueue(state,queue)
      {
        state.queue = queue
      }
  },
  actions: {

  }
})
