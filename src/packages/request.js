import store from '../store'
import axios from "axios"
import router from "../router"

export default function (Vue) {
    Vue.request = {
        logout(){
            localStorage.setItem('token', null)
            localStorage.setItem('user_status', 3)
            localStorage.setItem('user_user_id', null)
            store.commit('setStatus', null)
            router.push("/")
        },
        getToken() {
            return localStorage.getItem('token')
        },
        getHeader() {
            let header = {
                'headers': {
                    Authorization: this.getToken()
                }
            }
            return header
        },
        getStatus() {
            return store.state.status
        },

        getQueue(){
          return store.state.queue
        },

        getStatusFromLocalStorage()
        {
            return localStorage.getItem('user_status')
        },
        getStep() {
            if (store.state.step) {
                return store.state.step.step
            }
            else return 4
        },

        getUser() {
            return localStorage.getItem('user_user_id')
        },

        postRequest(callback, payload, url) {
            axios.post(url, payload, this.getHeader()).then(function (response) {

                    let status = response.data.user.status
                    let user_id = response.data.user.user_id
                    localStorage.setItem('token', response.data.token)
                    localStorage.setItem('user_status', status)
                    localStorage.setItem('user_user_id', user_id)
                    store.commit('setStatus',status)
                    callback(response.data.data)
                }
            ).catch(function (error) {
                if(error.response.data){
                if (error.response.data === "Unauthorized") {
                    localStorage.setItem('token', null)
                    localStorage.setItem('user_status', 3)
                    localStorage.setItem('user_user_id', null)
                    store.commit('setStatus', null)
                    router.push("/")
                }
                }
                callback(error)
            })
        },
        getRequest(callback, url) {
            axios.get(url, this.getHeader()).then(function (response) {

                let status = response.data.user.status
                let user_id = response.data.user.user_id
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user_status', status)
                localStorage.setItem('user_user_id', user_id)
                store.commit('setStatus',status)
                callback(response.data.data)
            }).catch(function (error) {
                if(error.response.data){
                if (error.response.data === "Unauthorized") {
                    localStorage.setItem('token', null)
                    localStorage.setItem('user_status', 3)
                    localStorage.setItem('user_user_id', null)
                    store.commit('setStatus', null)
                    router.push("/")
                }}
                callback(error)
            })
        },

        deleteRequest(callback, url) {
            axios.delete(url, this.getHeader()).then(function (response) {

                let status = response.data.user.status
                let user_id = response.data.user.user_id
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user_status', status)
                localStorage.setItem('user_user_id', user_id)
                store.commit('setStatus',status)
                callback(response.data.data)
            }).catch(function (error) {
                if(error.response.data){
                if (error.response.data === "Unauthorized") {
                    localStorage.setItem('token', null)
                    localStorage.setItem('user_status', 3)
                    localStorage.setItem('user_user_id', null)
                    store.commit('setStatus', null)
                    router.push("/")
                }}
                callback(error)
            })
        },

        putRequest(callback, payload, url) {
            axios.put(url, payload, this.getHeader()).then(function (response) {

                let status = response.data.user.status
                let user_id = response.data.user.user_id
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user_status', status)
                localStorage.setItem('user_user_id', user_id)
                store.commit('setStatus',status)
                callback(response.data.data)
            }).catch(function (error) {
                if(error.response.data){
                if (error.response.data === "Unauthorized") {
                    localStorage.setItem('token', null)
                    localStorage.setItem('user_status', 3)
                    localStorage.setItem('user_user_id', null)
                    store.commit('setStatus', null)
                    router.push("/")
                }}
                callback(error)
            })
        },

    }

    Object.defineProperties(Vue.prototype, {
        $request: {
            get: () => {
                return Vue.request
            }
        }
    })
}
