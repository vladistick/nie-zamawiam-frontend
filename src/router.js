import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import CurrentOrder from './views/CurrentOrder.vue'
import Restaurants from './views/Restaurants.vue'
import Restaurant from './views/Restaurant.vue'
import OrderHistory from './views/OrderHistory'
import PreviousOrder from './views/SpecificOrder'
import AdminRestaurants from './views/admin/AdminRestaurants'
import AdminRestaurantsProducts from './views/admin/AdminRestaurantsProducts'
import AdminProducts from './views/admin/AdminProducts'
import AdminUsers from './views/admin/AdminUsers'
import AdminConfirmUsers from './views/admin/AdminConfirmUsers'
import AdminBanUsers from './views/admin/AdminBanUsers'
import AdminAddRestaurant from './views/admin/AdminAddRestaurant'
import AdminAddProduct from './views/admin/AdminAddProduct'
import AdminEditRestaurant from './views/admin/AdminEditRestaurant'
import AdminEditProduct from './views/admin/AdminEditProduct'
import AccountNumber from './views/ChangeAccountNumber'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [

        {
            path: '/',
            name: 'login',
            component: Login,
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/history/:page',
            name: 'history',
            component: OrderHistory,
            meta: {
                forUsers: true
            }
        },
        {
            path: '/previous/:order_id',
            name: 'previous',
            component: PreviousOrder,
            meta: {
                forUsers: true
            }
        },
        {
            path: '/current',
            name: 'currentOrder',
            component: CurrentOrder,
            meta: {
                forUsers: true
            }
        },

        {
            path: '/restaurant/:restaurant_id',
            name: 'restaurant',
            component: Restaurant,
            meta: {
                forUsers: true
            }
        },
        {
            path: '/restaurants',
            name: 'restaurants',
            component: Restaurants,
            meta: {
                forUsers: true
            }
        },
        {
            path: '/admin/restaurants',
            name: 'admin-restaurants',
            component: AdminRestaurants,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/edit_restaurant/:restaurant_id',
            name: 'admin-restaurant',
            component: AdminEditRestaurant,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/add_restaurant',
            name: 'add_restaurant',
            component: AdminAddRestaurant,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/products/:restaurant_id',
            name: 'admin-products',
            component: AdminProducts,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/products',
            name: 'admin-restaurants-products',
            component: AdminRestaurantsProducts,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/edit_product/:product_id',
            name: 'edit-product',
            component: AdminEditProduct,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/add_product/:restaurant_id',
            name: 'add-product',
            component: AdminAddProduct,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/users',
            name: 'admin-users',
            component: AdminUsers,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/users/confirm',
            name: 'admin-confirm-users',
            component: AdminConfirmUsers,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/admin/users/ban',
            name: 'admin-ban-users',
            component: AdminBanUsers,
            meta: {
                forAdmins: true
            }
        },
        {
            path: '/user/account_number',
            name: 'change-account-number',
            component: AccountNumber,
            meta: {
                forUsers: true
            }
        },




    ]
})
