import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueEcho from 'vue-echo'
import Vuetify from 'vuetify'
import Request from './packages/request'
import 'vuetify/dist/vuetify.min.css'
import money from 'v-money'

const MOMENT = require('moment')
require('moment/locale/pl')

Vue.use(require('vue-moment'), {
    MOMENT
})
axios.defaults.baseURL = process.env.VUE_APP_WEBSITE_URL
Vue.use(money)
Vue.use(Request)
Vue.use(Vuetify)
Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(VueEcho, {
    broadcaster: 'pusher',
    key: process.env.VUE_APP_PUSHER_SECRET,

    cluster: 'eu',
    encrypted: true,
});


router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.request.getStatusFromLocalStorage()) {
                if (Vue.request.getStatusFromLocalStorage() <= 2) {
                    next({
                        path: '/current'
                    })
                } else next()
            } else next()
        }
        else if (to.matched.some(record => record.meta.forAdmins)) {
            if (!Vue.request.getStatusFromLocalStorage()) {
                next({
                    path: '/'
                })
            }
            else if (Vue.request.getStatusFromLocalStorage() > 2) {
                next({
                    path: '/'
                })
            }
            else if (Vue.request.getStatusFromLocalStorage() === 2) {
                next({
                    path: '/current'
                })
            }
            else next()
        }
        else if (to.matched.some(record => record.meta.forUsers)) {
            if (!Vue.request.getStatusFromLocalStorage()) {
                next({
                    path: '/'
                })
            }
            else if (Vue.request.getStatusFromLocalStorage() > 2) {
                next({
                    path: '/'
                })
            }
            else next()
        }
        else next()
    })

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
